fn main() {
    let mut s = String::from("hello");

    change(&mut s);
    println!("s {s}");

    let r = &mut s;
    println!("r {r}");

    change(&mut s);
    println!("s {s}");
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}